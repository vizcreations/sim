# Makefile for simulator program
# @author Webauro

SRC_CIL = sim.c
OBJ_CIL = sim.o

SRC_INCLUDES = -I/usr/local/include -I. -I/usr/include
SRC_LIBS = -L/usr/local/lib -I. -I/usr/lib

all: lib_cil compile install

lib_cil:
	gcc -c $(SRC_CIL)
	ar rcs sim.a $(OBJ_CIL)
	$(RM) *.o

compile:
	gcc -o Sim $(SRC_INCLUDES) main.c sim.a $(SRC_LIBS) -lc

install:


reset:
	rm Sim* *~

