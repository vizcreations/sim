/**
* @abstract A Formula One simulator program to determine car set up for optimal performance
* @copyright None
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sim.h"

char *teams[6] = { "Ferrari",
		"McLaren",
		"Lotus",
		"RedBull",
		"Mercedes",
		"Sauber" };

char *front_wings[] = {
			"1001a",
			"1001b",
			"2001b",
			"3003a",
			"4009x"
			};

int highFWAngle = 5.5;
int highRWAngle = 5.1;

int lowFWAngle = 0.1;
int lowRWAngle = 0.4;

int highFWClearance = 2.2;
int highRWClearance = 2.5;

int lowFWClearance = 1.2;
int lowRWClearance = 0.1;

int main(int argc, char *argv[], char *env[]) {
	char *buf;
	SIM simulator;
	SIM *simu = &simulator;
	if(argc < 4) {
		printf("\nUsage: %s <car> <action> <entity>\n", argv[0]);
		return -1;
	} else if(argc < 3) {
		printf("\nUsage: %s <car> <action> <entity>\n", argv[0]);
		return -1;
	} else if(argc < 2) {
		printf("\nUsage: %s <car> <action> <entity>\n", argv[0]);
		return -1;
	}

	if(strcmp(argv[1], "Ferrari") == 0) {
		if(strcmp(argv[2], "car") == 0) {
			strcpy(simu->car->team->name, argv[1]);
			strcpy(simu->car->engine->name, argv[1]);

			simu->car->engine->BHP = 800;
			simu->car->engine->CC = 12000;

			strcpy(simu->car->tyre->brand_name, "Bridgestone");
			strcpy(simu->car->tyre->prime_name, "Hard compound");
			strcpy(simu->drv->name, "Number One");
			strcpy(simu->car->chassis_name, "F40");
			strcpy(simu->trk->name, "Suzuka");

			/**
			* @save File detailing simulator's condition
			*
			*/
			buf = (char *) malloc(sizeof(char *) * 256);
			strcat(buf, argv[1]);
			strcat(buf, ".txt");
			buf[strlen(buf)+1] = '\0';
			FILE *fp = fopen(buf, "w");
			if(fp) {
				fprintf(fp, "Team: %s\n", simu->car->team->name);
				fprintf(fp, "BHP: %f\n", simu->car->engine->BHP);
				fprintf(fp, "CC: %f\n", simu->car->engine->CC);
				fprintf(fp, "Tyre: %s\n", simu->tyre->brand_name);
				fprintf(fp, "Track: %s\n", simu->trk->name);
				fclose(fp);
			}
			if(strcmp(argv[3], "describe") == 0) {
				printf("You have selected a Ferrari car, here's the car's details:\n");
				printf("Team: Ferrari\n");
				printf("BHP: 800\n");
				printf("CC: 10000\n");
				printf("Tyre: Bridgestone\n");
				printf("Engine displacement: %f\n", 525.98);
				printf("Drive No: %d\n", 2);
			}
		}
	}
	free(buf);
	free(simu);

	return 0;
}

