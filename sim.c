/**
* @abstract Library source file for simulator program
* @author Webauro
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sim.h"

void test_run(SIM *sim) {
	// TODO CODE
}

void strip_newline(char *str, int len) {
	int i = 0;
	for(i=0; i<len; i++) {
		if(str[i] == '\n') {
			str[i] = '\0';
			break;
		}
	}
}

