/**
* @abstract Header file for simulator program
* @copyright None
*/

struct FrontWing { // The simulator's front wing with values set
	int flex;
	char codename[20];
	int height;
	int response;
	float clearance;
	int angle;
};

struct RearWing {
	int flex;
	char codename[20];
	int height;
	int response;
	float clearance;
	int angle;
};

struct Engine {
	char name[20];
	float topspeed;
	float BHP;
	float CC;
};

struct GearBox {
	char name[20];
	int gears;
};

struct Suspension {
	char name[20];
};

struct SteeringColumn {
	char name[20];
	int buttons;
};

struct Weather {
	char type[50];
	float humidity;
	float rain_chance;
} wthr;

typedef struct Driver {
	char name[50];
	int age;
	int reflex_scale;
} DRV;

struct Team {
	char name[20];
	int stafftot;
};

struct Tyre {
	char brand_name[50];
	char prime_name[20];
	char opt_name[20];
	float wear_rate;
};

typedef struct Car {
	char chassis_name[20];
	struct Team *team;
	struct GearBox *gear_box;
	struct Engine *engine;
	struct Tyre *tyre;
} CAR;

struct Track {
	char name[50];
	int total_corners;
	char lap_record[100];
	float distance; // Kms
};

typedef struct Simulator {
	CAR *car;
	DRV *drv;
	struct Track *trk;
	struct Tyre *tyre;
	struct Weather *wthr;
	struct FrontWing *fw;
	struct RearWing *rw;
} SIM;

/** Function declarations */
void test_run(SIM *sim);
// Strip newlines from character string
void strip_newline(char *, int);
